import React from 'react';
// ctrl+kc
// import logo from './logo.svg';
import './App.css';
import Calculate from './calculate';
import Book from './books';
import Friend from './friend';
import Tabel from './tabel';
import Diskon from './diskon';
import Samp from './sample/index';
import Crud from './crud';
import Emp from './employee';

function App() {


  return (
    <div>
      <h1>Hello guys </h1>
      {/* <Calculate></Calculate>
      <hr/>
      <Tabel></Tabel>
      <Diskon></Diskon> */}
      <Book></Book>
      <hr/>
      <Emp></Emp>
      <Crud></Crud>
      {/* <Friend name="insert name" job="insert job"></Friend> 
      <Samp val1="one" val2="two"></Samp> */}
    </div>

  );
}

export default App;
