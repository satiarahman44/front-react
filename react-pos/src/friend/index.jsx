import React from 'react';

class Friend extends React.Component {
	values = {
		name: "insert name",
		job: "insert job"

	}
	constructor(props) {
		super(props);
		this.state = {
			values: this.values
		}
	}

	handlerChange = name => ({ target: { value } }) => {
		this.setState({
			values: {
				...this.state.values,
				[name]: value
			}
		});
	}

	render() {
		const { name, job } = this.state.values;
		return (
			<div>
				<h3>This is from {name}. </h3>
				<h3>My Profile </h3>
				<h5>{JSON.stringify(this.state.values)}</h5>
				<table>
					<tr>
						<td>Name</td>
						<td>: <input type="text" value={name} onChange={this.handlerChange('name')} /></td>
					</tr>
					<tr>
						<td>Job</td>
						<td>: <input type="text" value={job} onChange={this.handlerChange('job')} /></td>
					</tr>
				</table>
				<p>nama : {name}</p>
				<p>job : {job}</p>
			</div >
		)
	}
}

export default Friend;