import React from 'react';

class Add extends React.Component {
	render() {
		const { val1, val2 } = this.props;
		return (
			<div>
                <h3>Add: </h3>
                <h5>Result: { parseInt(val1) + parseInt(val2)}</h5>
			</div>
		)
	}
}

export default Add;