import React from 'react';
import Add from './add'
import Subtract from './min'
import Multiply from './multiply'
import Divide from './divide'

class Calculate extends React.Component {
	values = {
		val1: 1,
		val2: 2

	}
	constructor(props){
		super(props);
		this.state = {
			values: this.values
		}
	}

handlerChange = name => ({ target: {value}}) => {
	this.setState({
		values: {
			...this.state.values,
			[name]: value
		}
	})
}

	render() {
		const { val1, val2 } = this.state.values;
		return (
			<div>
				<h2>Calculation</h2>
				<h5>{ JSON.stringify(this.state.values)}</h5>
				<table>
					<tr>
						<td>
							Value 1: <input type="number" value={val1} onChange= {this.handlerChange('val1')}/>
						</td>
						<td>
							Value 2: <input type="number" value={val2} onChange= {this.handlerChange('val2')}/>
						</td>
					</tr>
					<tr>
						<Add val1={val1} val2={val2} />
					</tr>
					<tr>
						<Subtract val1={val1} val2={val2} />
					</tr>
					<tr>
						<Multiply val1={val1} val2={val2} />
					</tr>
					<tr>
						<Divide val1={val1} val2={val2} />
					</tr>
				</table>
			</div>
		)
	}
}

export default Calculate;