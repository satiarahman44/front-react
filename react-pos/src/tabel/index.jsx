import React from 'react';

class Tabel extends React.Component {
    values = {
        name: "write something here"

    }
    constructor(props) {
        super(props);
        this.state = {
            values: this.values
        }
    }

    handlerChange = name => ({ target: { value } }) => {
        this.setState({
            values: {
                ...this.state.values,
                [name]: value
            }
        });
    }

    render() {
        const { name } = this.state.values;
        return (
            <div>
                <h2>Tabel</h2>
                <p>{name}</p>
                <textarea name="" id="" cols="30" rows="10" value={name} onChange={this.handlerChange('name')}>
                    
                </textarea>
            </div>
        )
    }
}

export default Tabel;