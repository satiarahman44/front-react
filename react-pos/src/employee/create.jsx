import React from 'react';

class Create extends React.Component{
    constructor(props){
        super(props);
        this.setState({
            item: this.props.item,
            hidden: this.props.hidden
        })
    }

    render(){
        const { hidden, item:{id, firstname, lastname, age, salary, active}, handleChange, handleChangeCheckBox, toggleSaveCancel, handleSubmit} = this.props;
        return(
            <div hidden={hidden}>
                <table>
                    <tr>
                        <td>First Name</td><td><input type="input" value={firstname} onChange={handleChange('firstname')}/></td>
                    </tr>
                    <tr>
                        <td>Last Name</td><td><input type="input" value={lastname} onChange={handleChange('lastname')}/></td>
                    </tr>
                    <tr>
                        <td>Age</td><td><input type="input" value={age} onChange={handleChange('age')}/></td>
                    </tr>
                    <tr>
                        <td>Salary</td><td><input type="input" value={salary} onChange={handleChange('salary')}/></td>
                    </tr>
                    <tr>
                        <td> Active</td><td><input type="checkbox" checked={active} onChange={handleChangeCheckBox('active')}/></td>
                    </tr>
                    <tr>
                        <td>
                            <button type="button" onClick={handleSubmit} >Save</button>
                        </td>
                        <td>
                            <button type="button" onClick={toggleSaveCancel}>Cancel</button>
                        </td>
                    </tr>
                </table>
            </div>
        )
    }
}
export default Create;