import React from 'react';
import CreateEdit from './create';

class Employee extends React.Component {
    itemModel = {
        id: 0,
        firstname: '',
        lastname: ' ',
        age: ' ',
        salary: ' ',
        active: true
    }
    constructor(props) {
        super(props);
        this.state = {
            items: [   ],
            item: this.itemModel,
            hidden: true,
            createNew: false
        }
    }

    toggleCreate = () => {
        this.setState({
            hidden: false,
            item: this.itemModel,
            createNew: true

        })
    }

    toggleEdit = (id) => {
        const selectedItem = this.state.items.find(i => i.id === id);
        // console.log(selectedItem);
        this.setState({
            hidden: false,
            item: selectedItem,
            createNew: false
        });

    }
    toggleDelete = (id) => {
        const { items } = this.state;
        const selectedItem = this.state.items.find(i => i.id === id);
        if (window.confirm('Delete data for employee id '+selectedItem.id+'?')) {
        const idx = this.state.items.findIndex(i => i.id === id);
        items.splice(idx, 1);
        this.setState({
            items: items
        })
    }
}


    toggleSaveCancel = () => {
        this.setState({
            hidden: true
        })
    }



    handleChange = name => ({ target: { value } }) => {
        this.setState({
            item: {
                ...this.state.item,
                [name]: value
            }
        })
    }

    handleChangeCheckBox = name => event => {
        this.setState({
            item: {
                ...this.state.item,
                [name]: event.target.checked
            }
        })
    }
    handleSubmit = () => {
        const { item, items, createNew } = this.state;
        if (createNew) {
            //insert
            let newId = parseInt(Math.max(...items.map(i => i.id), 0))
                + 1;
            let newItem = {
                id: newId,
                firstname: item.firstname,
                lastname: item.lastname,
                age: item.age,
                salary: item.salary,
                active: item.active
            }

            items.push(newItem);

        } else {
            const idx = this.state.items.findIndex(i => i.id === item.id);
            items[idx] = {
                id: item.id,
                firstname: item.firstname,
                lastname: item.lastname,
                age: item.age,
                salary: item.salary,
                active: item.active
            }
        }

            this.setState({
                items: items,
                hidden: true,
                createNew: false
            })
        }
        render() {
            const { items, item, hidden } = this.state;
            return (
                <div>
                    <h3>List of Item</h3>

                    <button type="button" hidden={!hidden} onClick={this.toggleCreate}>Input employee data</button>
                    <CreateEdit hidden={hidden} item={item} toggleSaveCancel={this.toggleSaveCancel} handleChange={this.handleChange}
                        handleChangeCheckBox={this.handleChangeCheckBox} handleSubmit={this.handleSubmit} />
                    <br />
                    <table border="1">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Age</th>
                                <th>Salary</th>
                                <th>Active</th>
                            </tr>
                        </thead>
                        <tbody>
                            {items.map(i => {
                                return (
                                    <tr key={i.id}>
                                        <td>{i.id}</td>
                                        <td>{i.firstname}</td>
                                        <td>{i.lastname}</td>
                                        <td>{i.age}</td>
                                        <td>{i.salary}</td>
                                        <td><input type="checkbox" checked={i.active} /></td>
                                        <td>
                                            <button type="button" onClick={() => this.toggleEdit(i.id)}>Edit</button>
                                            <button type="button" onClick={() => this.toggleDelete(i.id)}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            }

                            )}

                        </tbody>
                    </table>
                </div>
            )
        }
    }
    export default Employee;