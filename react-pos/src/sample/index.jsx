import React from 'react';

class Sample extends React.Component{

render(){
	const { val1, val2 } = this.props
	return (
		<div>
			<h3>Write</h3>
			<h5>Something here!</h5>
			<p>1 ={val1} and 2 ={val2}</p>
		</div>)
}
}
export default Sample;