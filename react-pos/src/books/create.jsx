import React from 'react'

class Masuk extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        const {hidden, DBuku:{Title,Author,Publisher,Edition,Page,Year,Category,Price,Active},GantiIsiBox,GantiIsiData,Input,MunculButton, IlangButton} = this.props
        return(
            <tr hidden = {hidden}>
                <td align = "center"><input type = 'input' value={Title} onChange={GantiIsiData('Title')}/></td>
                <td align = "center"><input type = 'input' value={Author} onChange={GantiIsiData('Author')}/></td>
                <td align = "center"><input type = 'input' value={Publisher} onChange={GantiIsiData('Publisher')}/></td>
                <td align = "center"><input type = 'input' value={Edition} onChange={GantiIsiData('Edition')}/></td>
                <td align = "center"><input type = 'input' value={Page} onChange={GantiIsiData('Page')}/></td>
                <td align = "center"><input type = 'input' value={Year} onChange={GantiIsiData('Year')}/></td>
                <td align = "center"><input type = 'input' value={Category} onChange={GantiIsiData('Category')}/></td>
                <td align = "center"><input type = 'input' value={Price} onChange={GantiIsiData('Price')}/></td>
                <td align = "center"><input type = 'checkbox' checked={Active} onChange={GantiIsiBox('Active')}/></td>
                <td>
                    <button type = "button" onClick={Input}>Save</button>
                    <button type = "button" onClick={IlangButton}>Cancel</button>
                </td>
            </tr>
        )
    }
}

export default Masuk;