import React from 'react'
import Input from './create'

class Perpus extends React.Component {
    DetailBuku = {
        id: '',
        Title: '',
        Author: '',
        Publisher: '',
        Edition: '',
        Page: '',
        Year: '',
        Category: '',
        Price: '',
        Active: true
    }

    constructor(props) {
        super(props);
        this.state = {
            arrBuku: [
                { id: 1, Title: 'Buku Saya', Author: 'Bukan Saya', Publisher: 'Bukan saya juga', Edition: 'pertama dan terakhir', Page: 1, Year: 2000, Category: 'Iseng-iseng', Price: 0, Active: true }
            ],
            DBuku: this.DetailBuku,
            hidden: true,
            NewArrival: false
        }
    }

    MunculButton = () => {
        this.setState({
            hidden: false,
            NewArrival: true
        })
    }

    IlangButton = () => {
        this.setState({
            hidden: true,
            NewArrival: true
        })
    }

    Input = () => {
        const { DBuku, arrBuku, NewArrival } = this.state
        if (NewArrival) {
            let newID = parseInt(Math.max(...arrBuku.map(i => i.id), 0)) + 1;
            let newBook = {
                id: newID,
                Title: DBuku.Title,
                Author: DBuku.Author,
                Publisher: DBuku.Publisher,
                Edition: DBuku.Edition,
                Page: DBuku.Page,
                Year: DBuku.Year,
                Category: DBuku.Category,
                Price: DBuku.Price,
                Active: DBuku.Active
            }
            arrBuku.push(newBook);
        } else {
            const idx = this.state.arrBuku.findIndex(i => i.id === DBuku.id);
            arrBuku[idx] = {
                id: DBuku.id,
                Title: DBuku.Title,
                Author: DBuku.Author,
                Publisher: DBuku.Publisher,
                Edition: DBuku.Edition,
                Page: DBuku.Page,
                Year: DBuku.Year,
                Category: DBuku.Category,
                Price: DBuku.Price,
                Active: DBuku.Active
            }
        }

        this.setState({
            arrBuku: arrBuku,
            hidden: true
        })
    }

    GantiIsiData = name => event => {
        this.setState({
            DBuku: {
                ...this.state.DBuku,
                [name]: event.target.value
            }
        })
    }

    GantiIsiBox = name => event => {
        this.setState({
            DBuku: {
                ...this.state.DBuku,
                [name]: event.target.checked
            }
        })
    }

    DeleteBuku = (id) => {
        const { arrBuku } = this.state
        const PilihBuku = this.state.arrBuku.find(i => i.id === id)
        if (window.confirm('Yakin buku ' + PilihBuku.Title + ' mau diapus?')) {
            const idx = this.state.arrBuku.findIndex(i => i.id === id);
            arrBuku.splice(idx, 1);
            this.setState({
                arrBuku: arrBuku
            })
        }
    }

    EditBuku = (id) =>{
        const PilihBuku = this.state.arrBuku.find(i => i.id === id)
        this.setState({
            hidden: false,
            NewArrival: false,
            DBuku: PilihBuku
        })
    }






    render() {
        const { arrBuku, hidden, DBuku } = this.state;
        return (
            <div align="center">
                <h3>LIST BUKU DI SINI</h3>
                <button type="button" hidden={!hidden} onClick={this.MunculButton}>Input Buku Baru</button>
                <table border="1">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Publisher</th>
                            <th>Edition</th>
                            <th>Page</th>
                            <th>Year</th>
                            <th>Category</th>
                            <th>Price</th>
                            <th>Active</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <Input hidden = {hidden} IlangButton = {this.IlangButton} DBuku = {DBuku} GantiIsiBox = {this.GantiIsiBox} GantiIsiData = {this.GantiIsiData} Input = {this.Input}/>
                        {arrBuku.map(i => {
                            return (
                                <tr align="center" key={i.id}>
                                    <td align="center">{i.Title}</td>
                                    <td align="center">{i.Author}</td>
                                    <td align="center">{i.Publisher}</td>
                                    <td align="center">{i.Edition}</td>
                                    <td align="center">{i.Page}</td>
                                    <td align="center">{i.Year}</td>
                                    <td align="center">{i.Category}</td>
                                    <td align="center">{i.Price}</td>
                                    <td align="center"><input type="checkbox" checked={i.Active} /></td>
                                    <td align="center">
                                        <button type="button" onClick = {() => this.EditBuku(i.id)}>Edit</button>
                                        <button type="button" onClick = {() => this.DeleteBuku(i.id)}>Delete</button>
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }

}

export default Perpus;