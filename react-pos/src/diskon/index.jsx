import React from 'react';

class Diskon extends React.Component {
	values = {
		awal: "harga awal",
		diskon: "insert diskon (%) "

	}
	constructor(props) {
		super(props);
		this.state = {
			values: this.values
		}
	}

	handlerChange = name => ({ target: { value } }) => {
		this.setState({
			values: {
				...this.state.values,
				[name]: value
			}
		});
	}

	render() {
		const { awal, diskon } = this.state.values;
		return (
			<div>
				<h3>My Profile </h3>
				<h5>{JSON.stringify(this.state.values)}</h5>
				<table>
					<tr>
						<td>awal</td>
						<td>: <input type="number" value={awal} onChange={this.handlerChange('awal')} /></td>
					</tr>
					<tr>
						<td>diskon</td>
						<td>: <input type="number" value={diskon} onChange={this.handlerChange('diskon')} /></td>
					</tr>
				</table>
				<p>harga awal : {awal}</p>
				<p>diskon : {diskon}</p>
			</div >
		)
	}
}

export default Diskon;